package com.canoo.tableviewfx;

import javafx.event.Event;
import javafx.event.EventType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;


/**
 * Created by hansolo on 07.07.16.
 */
public class HoverEvent extends Event {
    public static final EventType<HoverEvent> CURRENT_ROW = new EventType(ANY, "currentRow");
    public static final EventType<HoverEvent> CURRENT_COL = new EventType(ANY, "currentCol");

    final public TableRow    TABLE_ROW;
    final public TableColumn TABLE_COL;


    public HoverEvent(final EventType<HoverEvent> TYPE, final TableRow ROW) {
        super(TYPE);
        TABLE_ROW = ROW;
        TABLE_COL = null;
    }
    public HoverEvent(final EventType<HoverEvent> TYPE, final TableColumn COL) {
        super(TYPE);
        TABLE_ROW = null;
        TABLE_COL = COL;
    }
}
