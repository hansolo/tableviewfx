/*
 * Copyright (c) 2016 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.canoo.tableviewfx;

import javafx.beans.DefaultProperty;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.BooleanPropertyBase;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ObjectPropertyBase;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.css.PseudoClass;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.ScrollToEvent;
import javafx.scene.control.SortEvent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TableView.ResizeFeatures;
import javafx.scene.control.TableView.TableViewFocusModel;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.util.Callback;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Comparator;


/**
 * Created by hansolo on 22.07.16.
 */
@DefaultProperty("children")
public class ExtendedTableViewFX<S> extends Region {
    private static final PseudoClass     FIRST_COLUMN_FIXED_PSEUDO_CLASS = PseudoClass.getPseudoClass("first-column-fixed");

    private              ObjectProperty<Callback<ResizeFeatures, Boolean>> columnResizePolicy;
    private              TableViewFX     fixedColumnTable;
    private              TableViewFX     mainTable;
    private              AnchorPane      pane;
    private              BooleanProperty firstColumnFixed;
    private              ScrollBar       horizontalScrollBar;


    // ******************** Constructors **************************************
    public ExtendedTableViewFX() { this(FXCollections.<S>observableArrayList()); }
    public ExtendedTableViewFX(final ObservableList<S> ITEMS) {
        fixedColumnTable = new TableViewFX(ITEMS);
        mainTable        = new TableViewFX(ITEMS);

        getStylesheets().add(TableViewFX.class.getResource("styles.css").toExternalForm());
        getStyleClass().add("extended-table-view-fx");

        firstColumnFixed   = new BooleanPropertyBase(false) {
            @Override protected void invalidated() { pseudoClassStateChanged(FIRST_COLUMN_FIXED_PSEUDO_CLASS, get()); }
            @Override public Object getBean() { return ExtendedTableViewFX.this; }
            @Override public String getName() { return "firstColumnFixed"; }
        };
        columnResizePolicy = new SimpleObjectProperty<>();
        columnResizePolicy = new ObjectPropertyBase<Callback<ResizeFeatures, Boolean>>() {
            @Override protected void invalidated() {
                //mainTable.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
                //fixedRowTable.setColumnResizePolicy(get());
                //fixedColumnTable.setColumnResizePolicy(get());

            }
            @Override public Object getBean() { return ExtendedTableViewFX.this; }
            @Override public String getName() { return "columnResizePolicy"; }
        };

        init();
        initGraphics();
        initBindings();
        registerListeners();
    }


    // ******************** Initialization ************************************
    private void init() {

    }

    private void initGraphics() {
        horizontalScrollBar = new ScrollBar();

        fixedColumnTable.getStyleClass().add("fixed-column-table");
        mainTable.getStyleClass().add("main-table");

        enableNode(fixedColumnTable, isFirstColumnFixed());

        AnchorPane.setTopAnchor(fixedColumnTable, 0d);
        AnchorPane.setBottomAnchor(fixedColumnTable, 0d);
        AnchorPane.setLeftAnchor(fixedColumnTable, 0d);

        AnchorPane.setTopAnchor(mainTable, 0d);
        AnchorPane.setRightAnchor(mainTable, 0d);
        AnchorPane.setBottomAnchor(mainTable, 0d);
        AnchorPane.setLeftAnchor(mainTable, 0d);

        AnchorPane.setRightAnchor(horizontalScrollBar, 0d);
        AnchorPane.setBottomAnchor(horizontalScrollBar, 0d);
        AnchorPane.setLeftAnchor(horizontalScrollBar, 0d);

        pane = new AnchorPane(fixedColumnTable, mainTable, horizontalScrollBar);

        getChildren().addAll(pane);
    }

    private void initBindings() {
        fixedColumnTable.itemsProperty().bind(mainTable.itemsProperty());

        fixedColumnTable.columnResizePolicyProperty().bind(mainTable.columnResizePolicyProperty());

        fixedColumnTable.rowFactoryProperty().bind(mainTable.rowFactoryProperty());

        fixedColumnTable.placeholderProperty().bind(mainTable.placeholderProperty());

        fixedColumnTable.selectionModelProperty().bind(mainTable.selectionModelProperty());

        fixedColumnTable.focusModelProperty().bind(mainTable.focusModelProperty());

        fixedColumnTable.editableProperty().bind(mainTable.editableProperty());

        fixedColumnTable.sortPolicyProperty().bind(mainTable.sortPolicyProperty());

        fixedColumnTable.onSortProperty().bind(mainTable.onSortProperty());
    }

    private void registerListeners() {
        widthProperty().addListener(o -> resize());
        heightProperty().addListener(o -> resize());

        mainTable.getColumns().addListener((ListChangeListener) change -> {
            if (mainTable.getColumns().size() == 0) return;

            fixedColumnTable.getColumns().setAll(mainTable.getColumns().get(0));
            ((SelectableTableColumn) fixedColumnTable.getColumns().get(0)).setPrefWidth(100);

            // Adjust AnchorPane layout
            AnchorPane.setTopAnchor(fixedColumnTable, mainTable.getFixedCellSize());
            AnchorPane.setBottomAnchor(fixedColumnTable, horizontalScrollBar.getHeight());

            AnchorPane.setTopAnchor(mainTable, mainTable.getFixedCellSize());
            AnchorPane.setBottomAnchor(mainTable, horizontalScrollBar.getHeight());
            AnchorPane.setLeftAnchor(mainTable, getColumns().get(0).getPrefWidth());
        });
        mainTable.fixedCellSizeProperty().addListener(o -> {
            double cellSize = mainTable.getFixedCellSize();
            if (isFirstColumnFixed()) {
                fixedColumnTable.setFixedCellSize(cellSize);
            }
        });
        mainTable.skinProperty().addListener((o, ov, nv) -> {
            if (null == mainTable.getScene()|| null == nv) return;

            AnchorPane.setBottomAnchor(fixedColumnTable, horizontalScrollBar.getHeight());
            AnchorPane.setBottomAnchor(mainTable, horizontalScrollBar.getHeight());
            AnchorPane.setLeftAnchor(mainTable, getColumns().get(0).getPrefWidth());

            // Setup scrolling
            ScrollBar[] scrollBars              = getScrollbars(mainTable);
            ScrollBar   mainHorizontalScrollBar = scrollBars[0];
            ScrollBar   mainVerticalScrollBar   = scrollBars[1];

            scrollBars = getScrollbars(fixedColumnTable);
            ScrollBar fixedColumnVerticalScrollBar = scrollBars[1];

            horizontalScrollBar.visibleProperty().addListener(o2 -> {
                if (horizontalScrollBar.isVisible()) {
                    AnchorPane.setBottomAnchor(fixedColumnTable, horizontalScrollBar.getHeight());
                    AnchorPane.setBottomAnchor(mainTable, horizontalScrollBar.getHeight());
                } else {
                    AnchorPane.setBottomAnchor(fixedColumnTable, 0d);
                    AnchorPane.setBottomAnchor(mainTable, 0d);
                }
            });

            mainVerticalScrollBar.visibleProperty().addListener(o2 -> AnchorPane.setRightAnchor(horizontalScrollBar, mainVerticalScrollBar.isVisible() ? mainVerticalScrollBar.getWidth() : 0d));

            mainHorizontalScrollBar.valueProperty().bindBidirectional(horizontalScrollBar.valueProperty());
            horizontalScrollBar.visibleProperty().bind(mainHorizontalScrollBar.visibleProperty());
            horizontalScrollBar.valueProperty().bindBidirectional(mainHorizontalScrollBar.valueProperty());
            fixedColumnVerticalScrollBar.valueProperty().bindBidirectional(mainVerticalScrollBar.valueProperty());
        });

        firstColumnFixed.addListener(o -> {
            if (fixedColumnTable.getColumns().size() == 0) return;
            if (isFirstColumnFixed()) {
                ((SelectableTableColumn) mainTable.getColumns().get(0)).getStyleClass().add("hidden-first-column");
                enableNode(fixedColumnTable, true);
            } else {
                ((SelectableTableColumn) mainTable.getColumns().get(0)).getStyleClass().remove("hidden-first-column");
                enableNode(fixedColumnTable, false);
            }
        });

        horizontalScrollBar.heightProperty().addListener(o -> {
            if (horizontalScrollBar.isVisible()) {
                AnchorPane.setBottomAnchor(fixedColumnTable, horizontalScrollBar.getHeight());
                AnchorPane.setBottomAnchor(mainTable, horizontalScrollBar.getHeight());
            } else {
                AnchorPane.setBottomAnchor(fixedColumnTable, 0d);
                AnchorPane.setBottomAnchor(mainTable, 0d);
            }
        });
    }


    // ******************** Public Methods ************************************
    @Override public ObservableList<Node> getChildren() { return super.getChildren(); }

    public boolean isFirstColumnFixed() { return firstColumnFixed.get(); }
    public void setFirstColumnFixed(final boolean FIXED) { firstColumnFixed.set(FIXED); }
    public BooleanProperty firstColumnFixedProperty() { return firstColumnFixed; }

    public ObservableList<S> getItems() {return mainTable.getItems(); }
    public void setItems(ObservableList<S> value) { itemsProperty().set(value); }
    public ObjectProperty<ObservableList<S>> itemsProperty() { return mainTable.itemsProperty(); }

    public boolean isTableMenuButtonVisible() { return mainTable.isTableMenuButtonVisible(); }
    public void setTableMenuButtonVisible (boolean value) { mainTable.setTableMenuButtonVisible(value); }
    public BooleanProperty tableMenuButtonVisibleProperty() { return mainTable.tableMenuButtonVisibleProperty(); }

    public Callback<ResizeFeatures, Boolean> getColumnResizePolicy() { return columnResizePolicy.get(); }
    public void setColumnResizePolicy(Callback<ResizeFeatures, Boolean> callback) { columnResizePolicy.set(callback); }
    public ObjectProperty<Callback<ResizeFeatures, Boolean>> columnResizePolicyProperty() { return columnResizePolicy; }

    public Callback<TableView<S>, TableRow<S>> getRowFactory() { return mainTable.getRowFactory(); }
    public void setRowFactory(Callback<TableView<S>, TableRow<S>> value) { mainTable.setRowFactory(value); }
    public ObjectProperty<Callback<TableView<S>, TableRow<S>>> rowFactoryProperty() { return mainTable.rowFactoryProperty(); }

    public Node getPlaceholder() { return mainTable.getPlaceholder(); }
    public void setPlaceholder(Node value) { mainTable.setPlaceholder(value); }
    public ObjectProperty<Node> placeholderProperty() { return mainTable.placeholderProperty(); }

    public TableViewSelectionModel<S> getSelectionModel() { return mainTable.getSelectionModel(); }
    public void setSelectionModel(TableViewSelectionModel<S> value) { mainTable.setSelectionModel(value); }
    public ObjectProperty<TableViewSelectionModel<S>> selectionModelProperty() { return mainTable.selectionModelProperty(); }

    public TableViewFocusModel<S> getFocusModel() { return mainTable.getFocusModel(); }
    public void setFocusModel(TableViewFocusModel<S> value) { mainTable.setFocusModel(value); }
    public ObjectProperty<TableViewFocusModel<S>> focusModelProperty() { return mainTable.focusModelProperty(); }


//    // --- Span Model
//    private ObjectProperty<SpanModel<S>> spanModel
//            = new SimpleObjectProperty<SpanModel<S>>(this, "spanModel") {
//
//        @Override protected void invalidated() {
//            ObservableList<String> styleClass = getStyleClass();
//            if (getSpanModel() == null) {
//                styleClass.remove(CELL_SPAN_TABLE_VIEW_STYLE_CLASS);
//            } else if (! styleClass.contains(CELL_SPAN_TABLE_VIEW_STYLE_CLASS)) {
//                styleClass.add(CELL_SPAN_TABLE_VIEW_STYLE_CLASS);
//            }
//        }
//    };
//
//    public final ObjectProperty<SpanModel<S>> spanModelProperty() {
//        return spanModel;
//    }
//    public final void setSpanModel(SpanModel<S> value) {
//        spanModelProperty().set(value);
//    }
//
//    public final SpanModel<S> getSpanModel() {
//        return spanModel.get();
//    }

    public boolean isEditable() { return mainTable.isEditable(); }
    public void setEditable(boolean value) { mainTable.setEditable(value); }
    public BooleanProperty editableProperty() { return mainTable.editableProperty(); }

    public double getFixedCellSize() { return mainTable.getFixedCellSize(); }
    public void setFixedCellSize(double value) { mainTable.setFixedCellSize(value); }
    public DoubleProperty fixedCellSizeProperty() { return mainTable.fixedCellSizeProperty(); }

    public Comparator<S> getComparator() { return mainTable.getComparator(); }
    public ReadOnlyObjectProperty<Comparator<S>> comparatorProperty() { return mainTable.comparatorProperty(); }

    public Callback<TableView<S>, Boolean> getSortPolicy() { return mainTable.getSortPolicy(); }
    public void setSortPolicy(Callback<TableView<S>, Boolean> callback) { mainTable.setSortPolicy(callback); }
    public ObjectProperty<Callback<TableView<S>, Boolean>> sortPolicyProperty() { return mainTable.sortPolicyProperty(); }

    public EventHandler<SortEvent<TableView<S>>> getOnSort() { return mainTable.getOnSort(); }
    public void setOnSort(EventHandler<SortEvent<TableView<S>>> value) { mainTable.setOnSort(value); }
    public ObjectProperty<EventHandler<SortEvent<TableView<S>>>> onSortProperty() { return mainTable.onSortProperty(); }

    public ObservableList<TableColumn<S,?>> getColumns() { return mainTable.getColumns(); }

    public ObservableList<TableColumn<S,?>> getSortOrder() {return mainTable.getSortOrder(); }

    public void scrollTo(int index) { mainTable.scrollTo(index); }
    public void scrollTo(S object) { mainTable.scrollTo(object); }

    public EventHandler<ScrollToEvent<Integer>> getOnScrollTo() { return mainTable.getOnScrollTo(); }
    public void setOnScrollTo(EventHandler<ScrollToEvent<Integer>> value) { mainTable.setOnScrollTo(value); }
    public ObjectProperty<EventHandler<ScrollToEvent<Integer>>> onScrollToProperty() { return mainTable.onScrollToProperty(); }

    public void scrollToColumn(TableColumn<S, ?> column) { mainTable.scrollToColumn(column); }

    public void scrollToColumnIndex(int columnIndex) { mainTable.scrollToColumnIndex(columnIndex); }

    public EventHandler<ScrollToEvent<TableColumn<S, ?>>> getOnScrollToColumn() { return mainTable.getOnScrollToColumn(); }
    public void setOnScrollToColumn(EventHandler<ScrollToEvent<TableColumn<S, ?>>> value) { mainTable.setOnScrollToColumn(value); }
    public ObjectProperty<EventHandler<ScrollToEvent<TableColumn<S, ?>>>> onScrollToColumnProperty() { return mainTable.onScrollToColumnProperty(); }

    public boolean resizeColumn(TableColumn<S,?> column, double delta) {
        fixedColumnTable.resizeColumn(column, delta);
        return mainTable.resizeColumn(column, delta);
    }

    public void edit(int row, TableColumn<S,?> column) { mainTable.edit(row, column); }

    public ObservableList<TableColumn<S,?>> getVisibleLeafColumns() { return mainTable.getVisibleLeafColumns(); }

    public int getVisibleLeafIndex(TableColumn<S,?> column) { return mainTable.getVisibleLeafIndex(column); }

    public TableColumn<S,?> getVisibleLeafColumn(int column) { return mainTable.getVisibleLeafColumn(column); }

    public void sort() {
        fixedColumnTable.sort();
        mainTable.sort();
    }

    public void refresh() {
        fixedColumnTable.refresh();
        mainTable.refresh();
    }


    // ******************** Private Methods ***********************************
    private ScrollBar[] getScrollbars(final TableView TABLE_VIEW) {
        ScrollBar[] result = new ScrollBar[2];
        for (Node node : TABLE_VIEW.lookupAll(".scroll-bar")) {
            if (node instanceof ScrollBar) {
                ScrollBar bar = (ScrollBar) node;
                switch(bar.getOrientation()) {
                    case HORIZONTAL: result[0] = bar; break;
                    case VERTICAL  : result[1] = bar; break;
                }
            }
        }
        return result;
    }

    private void enableNode(final Node NODE, final boolean ENABLE) {
        NODE.setVisible(ENABLE);
        NODE.setManaged(ENABLE);
    }

    private void resize() {
        double width  = getWidth() - getInsets().getLeft() - getInsets().getRight();
        double height = getHeight() - getInsets().getTop() - getInsets().getBottom();
        pane.setPrefSize(width, height);
    }

    private static Object deepClone(final Object OBJECT) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream    objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(OBJECT);

            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
            ObjectInputStream    objectInputStream = new ObjectInputStream(byteArrayInputStream);

            return objectInputStream.readObject();
        }
        catch (Exception e) {
            System.out.println("Error cloning object: " + e.toString());
            return null;
        }
    }
}
